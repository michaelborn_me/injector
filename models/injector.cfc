/**
 * Playing with DI
 */
component accessors="true" {
	/**
	 * Try to load items from this dir by default
	*/
	property name="baseDir";

	public component function init( required string baseDir ) {
		setBaseDir( arguments.baseDir );
		return this;
	}

	public function inject( required string slug ) {
		var data = parseSlug( arguments.slug );
		// TODO: Load this on startup or something instead of on the fly
		var model = new "#data.namespace#"();
		parseComponentMeta( model );
		return model;
	}

	private function parseSlug( required string slug ) {
		if ( arguments.slug CONTAINS "@" ) {
			var path = arguments.slug.listGetAt(2, "@");
			var filename = arguments.slug.listGetAt(1, "@");
			return {
				path: getModelDir( path ),
				namespace: getModelNamespace( path, filename ),
				filename: filename
			};
		} else if ( arguments.slug CONTAINS "/" ) {
			var path = arguments.slug;
			var filename = arguments.slug.listLast("/");
			var namespace = arguments.slug.left( arguments.slug.len() - len("/" & filename));
			return {
				path: getModelDir( arguments.slug ),
				namespace: getModelNamespace( namespace, filename ),
				filename: filename
			};
		} else {
			var filename = arguments.slug.listLast( "." );
			var namespace = arguments.slug.left( arguments.slug.len() - len("." & filename));
			return {
				path: getModelDir( arguments.slug ),
				namespace: getModelNamespace( namespace, filename ),
				filename: filename
			};
		}
	}

	private function parseComponentMeta( component model ) {
		// bla
		var meta = getMetadata( model );
		// check "singleton" meta key, meta.keyExists("singleton")
		// check "scope" meta key/val, meta.keyExists("scope") && meta.scope > ""
		meta.properties.each( (item) => {
			if ( item.keyExists("inject") ) {
				// this is gonna get hair quick...
				// it's recursively instantiating new classes.
				// TODO: Save instantiated classes in an array property,
				// then return that IF it is singleton and already created.
				model["set#item.name#"]( inject( item.inject ) );
			}
		} );
	}

	private string function getModelDir( required string path ) {
		return ExpandPath( "/" & getBaseDir() & "/" & arguments.path );
	}

	private string function getModelNamespace( required string path, required string filename ) {
		return getBaseDir().replace("/", ".", "ALL") & "." & arguments.path & "." & arguments.filename;
	}
}