/**
* This tests the BDD functionality in TestBox. This is CF10+, Railo4+
*/
component extends="testbox.system.BaseSpec"{

/*********************************** LIFE CYCLE Methods ***********************************/

	function beforeAll(){
		application.injector = new plugins.injector.models.injector( "plugins" );
	}

	function afterAll(){
		structClear( application );	
	}

/*********************************** BDD SUITES ***********************************/

	function run(){

		/** 
		* describe() starts a suite group of spec tests.
		* Arguments:
		* @title The title of the suite, Usually how you want to name the desired behavior
		* @body A closure that will resemble the tests to execute.
		* @labels The list or array of labels this suite group belongs to
		* @asyncAll If you want to parallelize the execution of the defined specs in this suite group.
		* @skip A flag that tells TestBox to skip this suite group from testing if true
		*/
		describe( "Injector", function(){
		
			// before each spec in THIS suite group
			beforeEach(function(){
				coldbox = 0;
				coldbox++;
			});
			
			// after each spec in THIS suite group
			afterEach(function(){
				foo = 0;
			});
			it("can load components by slug and package name", function(){
				expect( 
					application.injector.inject( "injector@models" )
				 ).toBeComponent();
			});
			it("can load components by dotted path", function(){
				expect( 
					application.injector.inject( "models.injector" )
				 ).toBeComponent();
			});
			it("can load components by slash path", function(){
				expect( 
					application.injector.inject( "models/injector" )
				 ).toBeComponent();
			});
			it("will throw error if model not found", function(){
				expect( () => {
					application.injector.inject( "nothing@models" )
				} ).toThrow( );
			});
			it("can inject components into component properties", function(){
				expect( 
					application.injector.inject( "tests/models/testit" ).getEventLib()
				 ).toBeComponent();
			});
		});


	}

}